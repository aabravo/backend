# Image docker base
FROM node

# Definimos directorio de trabajo de docker
WORKDIR /docker-uruguay

# Copiar archivos del proyecto al WORKDIR
ADD . /docker-uruguay

# Instala las dependencias del proyecto
#RUN npm install

# Puerto donde exponemos el contenedor
EXPOSE 3000

# Comando para lanzar la app
CMD ["npm" , "start"]
