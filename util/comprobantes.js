var generaHTML = require('../util/generaHTML.js');
var htmlpdf = require('html-pdf');

function crear(datos,nombre){
  var html = generaHTML.genera(datos);
  var options = { format: 'Letter' };
  let arch = './comprobantes/' + nombre + '.pdf';
  htmlpdf.create(html, options).toFile(arch, function(err, res) {
    if (err) return console.log(err);
  });
}

module.exports.crear = crear;
