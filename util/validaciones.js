var bodyParser = require('body-parser');
var requestJSON = require('request-json');


//Datos para conexion a MLab
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techubduruguay/';
var apiKey = 'apiKey=aGkVpuk06S-C6_DxgNiZdbsySpZa4QVd';


function validarDatosLogin(login){
  var resultado = {};
  var errores = [];
  var ok = true;
  //Valido password
  if (login.password == undefined){
    ok = false;
    errores.push({"error":"Debe indicar password"});
  }
  //Valido email
  if (login.email == undefined){
    ok = false;
    errores.push({"error":"Debe indicar email"});
  }
  resultado.ok = ok;
  resultado.errores = errores;
  return resultado;
}

function validarAccount(account){
  var resultado = {};
  var errores = [];
  var ok = true;
  //Valido nombre
  if (account.nombre == undefined){
    ok = false;
    errores.push({"error":"Debe indicar el nombre de la cuenta"});
  }
  //Valido moneda
  if (account.moneda == undefined){
    ok = false;
    errores.push({"error":"Debe indicar la moneda de la cuenta"});
  }else if(!validateCurrencyCode(account.moneda)){
    ok = false;
    errores.push({"error":"El código de moneda no es válido"});
  }
  if(account.saldo != undefined && isNaN(account.saldo)){
    ok = false;
    errores.push({"error":"El saldo debe ser numérico"});
  }
  resultado.ok = ok;
  resultado.errores = errores;
  return resultado;
}

function validarDatosLogin(login){
  var resultado = {};
  var errores = [];
  var ok = true;
  //Valido password
  if (login.password == undefined){
    ok = false;
    errores.push({"error":"Debe indicar password"});
  }
  //Valido email
  if (login.email == undefined){
    ok = false;
    errores.push({"error":"Debe indicar email"});
  }
  resultado.ok = ok;
  resultado.errores = errores;
  return resultado;
}

function validarDatosTransaccion(transaction){
  var resultado = {};
  var errores = [];
  var ok = true;

  //Valido Monto
  if (transaction.monto == undefined){
    ok = false;
    errores.push({"error":"Debe indicar el monto  de la transacción"});
  }else if(isNaN(transaction.monto)){
    ok = false;
    errores.push({"error":"El monto de la transacción debe ser numérico"});
  }
  //Valido Tipo
  if(transaction.tipo == undefined || (transaction.tipo!='DEBITO' && transaction.tipo!='CREDITO')){
    ok = false;
    errores.push({"error":"Debe indicar el tipo de trsacción (DEBITO O CREDITO)"});
  }
  //Valido el Concepto
  if(transaction.concepto == undefined || (transaction.tipo.length == 0)){
    ok = false;
    errores.push({"error":"Debe indicar el concepto de la transacción"});
  }
  resultado.ok = ok;
  resultado.errores = errores;
  return resultado;
}



module.exports.validarDatosLogin = validarDatosLogin;
module.exports.validarAccount = validarAccount;
module.exports.validarDatosTransaccion = validarDatosTransaccion;
