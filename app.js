//Datos para conexion a MLab
var config = require('./config');
var port = config.port; //process.env.PORT || 3000;
const URI = config.URI; // '/api-uruguay/v1/';

var express = require('express');
var bodyParser = require('body-parser');

/** Controladores del api Rest */
var userController = require('./controllers/users.js');
var accountController = require('./controllers/accounts.js');
var transactionsController = require('./controllers/transactions.js');
var loginController = require('./controllers/login.js');
var middController = require('./controllers/middleware.js');
var listaNegraJWT = require('./model/ListaNegraJWT.js');
var jwt = require('jsonwebtoken');

var app = express();
app.use(bodyParser.json());

app.listen(port);
console.log('Servidor Activo...');

/************************/
/*      MIDDLEWARE      */
/************************/
app.use(middController.middleware);


/************************/
/*      USER            */
/************************/
// GET USER/
app.get(URI + 'users', userController.get);
// GET USER/{ID}
app.get(URI + 'users/:id',userController.get_id);
// POST USER/
app.post(URI + 'users', userController.post);
// PUT USER/{ID}
app.put(URI + 'users/:id',userController.put_id);
// DELETE USER/{ID}
app.delete(URI + 'users/:id', userController.delete_id);

/*******************/
/* ACCOUNTS        */
/*******************/
// POST USER/{ID}/ACCOUNTS
app.post(URI + 'users/:id/accounts',accountController.post);
// GET USER/{ID}/ACCOUNTS
app.get(URI + 'users/:id/accounts', accountController.get);
// GET USER/{ID}/ACCOUNTS/{ID_ACCOUNT}
app.get(URI + 'users/:id/accounts/:id_account', accountController.get);
// PUT USER/{ID}/ACCOUNTS/{ID_ACCOUNT}
app.put(URI + 'users/:id/accounts/:id_account', accountController.putAccount);
// DELETE USER/{ID}/ACCOUNTS/{ID_ACCOUNT}
app.delete(URI + 'users/:id/accounts/:id_account', accountController.deleteAccount);

/*******************/
/* TRANSACTIONS    */
/*******************/
// POST USER/{ID}/ACCOUNTS/{ID_ACCOUNT}/TRANSACTION
app.post(URI + 'users/:id/accounts/:id_account/transactions',transactionsController.post);
// GET USER/{ID}/ACCOUNTS/{ID_ACCOUNT}/TRANSACTION
app.get(URI + 'users/:id/accounts/:id_account/transactions',transactionsController.get);
// GET USER/{ID}/ACCOUNTS/{ID_ACCOUNT}/TRANSACTION/{ID_TRANSACTION}
app.get(URI + 'users/:id/accounts/:id_account/transactions/:numero',transactionsController.get);
// GET USER/{ID}/ACCOUNTS/{ID_ACCOUNT}/TRANSACTION/{ID_TRANSACTION}/PDF
app.get(URI + 'users/:id/accounts/:id_account/transactions/:numero/pdf',transactionsController.archivo);
// PUT USER/{ID}/ACCOUNTS/{ID_ACCOUNT}/TRANSACTION/{ID_TRANSACTION}
app.put(URI + 'users/:id/accounts/:id_account/transactions/:numero',transactionsController.operacionNoValida);
// DELETE USER/{ID}/ACCOUNTS/{ID_ACCOUNT}/TRANSACTION/{ID_TRANSACTION}
app.delete(URI + 'users/:id/accounts/:id_account/transactions/:numero',transactionsController.operacionNoValida)

/************************/
/*      LOGIN           */
/************************/
// POST LOGIN/
app.post(URI + 'login', loginController.post);
// DELETE LOGIN
app.delete(URI + 'login',loginController.logout);
